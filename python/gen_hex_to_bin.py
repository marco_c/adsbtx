#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 gr-ADSBTx author.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import sys
import time
import csv
import numpy as np
from gnuradio import gr
import pmt

Tasa_Simbolos = 1e6  # simbolos/segundo
bits_datos = 112

class gen_hex_to_bin(gr.sync_block):
    """
    docstring for block gen_hex_to_bin
    """
    def __init__(self, fs, file):
        gr.sync_block.__init__(self,
            name="gen_hex_to_bin",
            in_sig=None,
            out_sig=[np.int8])

        self.fs = fs
        assert self.fs % Tasa_Simbolos == 0, "ADS-B está diseñado para operar en un número entero de muestras por símbolo, no% f sps" % (self.fs / Tasa_Simbolos)
        self.rmuestras = int(fs // Tasa_Simbolos) #Calculo la relacion entre samp_rate y la Tasa de Simbolos
        self.file=file

        f = open(self.file, "rt")
        #cambiar la direccion del directorio por la que se quiera generar el arcivo csv
        f2 = open("/home/marco/Documentos/Capturamarcosm.csv","wt")

        mensaje=f.readlines()

        lineas=[]
        tramas=[]
        nro_linea=1

        for x in mensaje:   
            if x[1]=='8' and x[2]=='D':
                for i in range(len(x)-3):
                    lineas.append(x[i+1])


        for i in range(len(lineas)):
            f2.write(lineas[i])
        
            if i==nro_linea*(27) and nro_linea == 1:
                nro_linea = nro_linea + 1
                f2.write("\n")
            elif i==nro_linea*(28)-1 and nro_linea !=1:
                f2.write("\n")
                nro_linea = nro_linea + 1
            else:
                f2.write(',')
        f2.close()

        f3=open("/home/marco/Documentos/Capturamarcosm.csv","rt")
        self.mensaje=f3.read()

        self.contador=0
        self.p1=0

        # Establezca el historial para que podamos verificar los preámbulos que envuelven el
        # fin de input_items de la llamada work () anterior [0]
        self.N_hist = bits_datos*self.rmuestras
        self.set_history(self.N_hist)

        # Propagar etiquetas
        self.set_tag_propagation_policy(gr.TPP_DONT)


    def work(self, input_items, output_items):
        out = output_items[0]
        
        b1=0
        b2=1
        b3=2
        b4=3
        N = len(output_items[0])
        pulso=[]
        trama=np.zeros(28,dtype=str)
        codificacion=np.zeros(112,dtype=int)
        if self.contador < len(self.mensaje)/55:
            for i in range(28):
                trama[i]=self.mensaje[self.p1]
                self.p1=self.p1+2
            for i in range(len(trama)):
                if trama[i]=='0':
                    codificacion[b1]=0
                    codificacion[b2]=0
                    codificacion[b3]=0
                    codificacion[b4]=0
                if trama[i]=='1':
                    codificacion[b1]=0
                    codificacion[b2]=0
                    codificacion[b3]=0
                    codificacion[b4]=1
                if trama[i]=='2':
                    codificacion[b1]=0
                    codificacion[b2]=0
                    codificacion[b3]=1
                    codificacion[b4]=0
                if trama[i]=='3':
                    codificacion[b1]=0
                    codificacion[b2]=0
                    codificacion[b3]=1
                    codificacion[b4]=1
                if trama[i]=='4':
                    codificacion[b1]=0
                    codificacion[b2]=1
                    codificacion[b3]=0
                    codificacion[b4]=0
                if trama[i]=='5':
                    codificacion[b1]=0
                    codificacion[b2]=1
                    codificacion[b3]=0
                    codificacion[b4]=1
                if trama[i]=='6':
                    codificacion[b1]=0
                    codificacion[b2]=1
                    codificacion[b3]=1
                    codificacion[b4]=0
                if trama[i]=='7':
                    codificacion[b1]=0
                    codificacion[b2]=1
                    codificacion[b3]=1
                    codificacion[b4]=1
                if trama[i]=='8':
                    codificacion[b1]=1
                    codificacion[b2]=0
                    codificacion[b3]=0
                    codificacion[b4]=0
                if trama[i]=='9':
                    codificacion[b1]=1
                    codificacion[b2]=0
                    codificacion[b3]=0
                    codificacion[b4]=1
                if trama[i]=='A' or trama[i]=='a':
                    codificacion[b1]=1
                    codificacion[b2]=0
                    codificacion[b3]=1
                    codificacion[b4]=0
                if trama[i]=='B' or trama[i]=='b':
                    codificacion[b1]=1
                    codificacion[b2]=0
                    codificacion[b3]=1
                    codificacion[b4]=1
                if trama[i]=='C' or trama[i]=='c':
                    codificacion[b1]=1
                    codificacion[b2]=1
                    codificacion[b3]=0
                    codificacion[b4]=0
                if trama[i]=='D' or trama[i]=='d':
                    codificacion[b1]=1
                    codificacion[b2]=1
                    codificacion[b3]=0
                    codificacion[b4]=1
                if trama[i]=='E' or trama[i]=='e':
                    codificacion[b1]=1
                    codificacion[b2]=1
                    codificacion[b3]=1
                    codificacion[b4]=0
                if trama[i]=='F' or trama[i]=='f':
                    codificacion[b1]=1
                    codificacion[b2]=1
                    codificacion[b3]=1
                    codificacion[b4]=1
                b1=b1+4
                b2=b2+4
                b3=b3+4
                b4=b4+4

        for i in range(N):
            if i < 40:
                pulso.append(0)
            if i == 40:
                self.add_item_tag(
                   0,
                    (self.nitems_written(0)) +i,
                    pmt.to_pmt("burst"),
                    pmt.to_pmt("gen1")
                )
                pulso.append(codificacion[i-40])
            if i > 40 and i <= (40 + len(codificacion)-1):
                pulso.append(codificacion[i-40])
            if i > 40 + len(codificacion) and i <= N:
                pulso.append(0)
        if N - len(pulso) != 0:
            pulso.append(np.zeros(N-len(pulso))) 
        
        self.contador=self.contador+1
        
        signal = []
        for bit in pulso:
            if bit == 1:
                I = 1
                Q = 1
            else:
                I = 0
                Q = 0
            signal.append(I)
            signal.append(Q)
            
        #out[:] = pulso
        out[:]= signal
        return len(output_items[0])

