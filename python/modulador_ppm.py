#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 gr-ADSBTx author.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy as np
from gnuradio import gr
import pmt

Tasa_Simbolos = 2e6  # simbolos/segundo
bits_preambulo = 8
bits_datos = 112
pulsos_datos= 224
pulsos_preambulo = bits_preambulo*2
pulsos_totales = pulsos_preambulo + pulsos_datos

class modulador_ppm(gr.interp_block):
    """
    docstring for block modulador_ppm
    """
    def __init__(self, fs, threshold, interpolation):
        gr.interp_block.__init__(self,
            name="modulador_ppm",
            in_sig=[np.int8],
            out_sig=[np.int8],
            interp=interpolation)

        self.fs = fs
        assert self.fs % Tasa_Simbolos == 0, "ADS-B está diseñado para operar en un número entero de muestras por símbolo, no% f sps" % (self.fs / Tasa_Simbolos)
        self.rmuestras = int(fs // Tasa_Simbolos) #Calculo la relacion entre samp_rate y la Tasa de Simbolos
        self.threshold = threshold
        self.interpolation = interpolation

        self.straddled_packet = 0

        self.set_tag_propagation_policy(gr.TPP_ONE_TO_ONE)
        self.message_port_register_out(pmt.to_pmt("Modulado"))

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # Si hubo un paquete que se extendió al bloque anterior y este
        # block, termina de decodificarlo
        if self.straddled_packet == 1:
            self.straddled_packet = 0
        
        #busco etiquetas del bloque anterior
        tags = self.get_tags_in_window(0, 0, len(in0), pmt.to_pmt("burst"))
        #Genero una lista de ceros del mismo tamaño que el numero de salidas para no tener problemas de longitudes
        pulso = np.zeros(len(out), dtype=int)

        p1=0
        p2=self.rmuestras

        for tag in tags:
            # Obtener metadatos para esta etiqueta
            value = pmt.to_python(tag.value)
            
            # Calcule las compensaciones de SOB y EOB
            sob_offset = tag.offset # Inicio del índice de ráfaga cuando recibo el primer bit del dato
            eob_offset = tag.offset + (bits_datos - 1)*self.rmuestras # Fin del índice de ráfaga 
            
            # Encuentre los índices SOB y EOB en este bloque de muestras
            sob_idx = sob_offset - self.nitems_written(0)/(2*self.rmuestras)
            eob_idx = eob_offset - self.nitems_written(0)/(2*self.rmuestras)

            if eob_idx  <= len(input_items[0]):
                # El paquete está completamente dentro de este bloque de muestras, así que module
                # toda la ráfaga
                for i in range(len(input_items[0])):
                    if i == sob_idx:
                        #dejo el espacio para colocar el preambulo
                        p1=sob_idx+16*self.rmuestras
                        p2=p2+sob_idx+16*self.rmuestras
                        #coloco el preambulo
                        pulso[i:i+self.rmuestras:1]=1
                        pulso[i+self.rmuestras:i+2*self.rmuestras:1]=0
                        pulso[i+2*self.rmuestras:i+3*self.rmuestras:1]=1
                        pulso[i+3*self.rmuestras:i+4*self.rmuestras:1]=0
                        pulso[i+4*self.rmuestras:i+5*self.rmuestras:1]=0
                        pulso[i+5*self.rmuestras:i+6*self.rmuestras:1]=0
                        pulso[i+6*self.rmuestras:i+7*self.rmuestras:1]=0
                        pulso[i+7*self.rmuestras:i+8*self.rmuestras:1]=1
                        pulso[i+8*self.rmuestras:i+9*self.rmuestras:1]=0
                        pulso[i+9*self.rmuestras:i+10*self.rmuestras:1]=1
                        pulso[i+10*self.rmuestras:i+11*self.rmuestras:1]=0
                        pulso[i+11*self.rmuestras:i+12*self.rmuestras:1]=0
                        pulso[i+12*self.rmuestras:i+13*self.rmuestras:1]=0
                        pulso[i+13*self.rmuestras:i+14*self.rmuestras:1]=0
                        pulso[i+14*self.rmuestras:i+15*self.rmuestras:1]=0
                        pulso[i+15*self.rmuestras:i+16*self.rmuestras:1]=0
                        # a continuacion modulo los 112 bits siguientes
                        if in0[i]>self.threshold:
                            pulso[p1:p1+self.rmuestras:1]=1
                            pulso[p2:p2+self.rmuestras:1]=0
                        elif input_items[i]<self.threshold:
                            pulso[p1:p1+self.rmuestras:1]=0
                            pulso[p2:p2+self.rmuestras:1]=1
                        p1=p1+2*self.rmuestras
                        p2=p2+2*self.rmuestras
                    if i > sob_idx and i  <= eob_idx:
                        if in0[i]>self.threshold:
                            pulso[p1:p1+self.rmuestras:1]=1
                            pulso[p2:p2+self.rmuestras:1]=0
                        elif in0[i]<self.threshold:
                            pulso[p1:p1+self.rmuestras:1]=0
                            pulso[p2:p2+self.rmuestras:1]=1
                        p1=p1+2*self.rmuestras
                        p2=p2+2*self.rmuestras
            else:
                # El paquete solo está contenido parcialmente en este bloque de
                # muestras, decodificar tanto como sea posible
                self.straddled_packet = 1    
            

        out[:] = pulso
        return len(output_items[0])

