#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2020 gr-ADSBTx author.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import sys
import time
import csv
import math
import numpy as np
from gnuradio import gr
import pmt

Tasa_Simbolos = 1e6  # simbolos/segundo
bits_datos = 112

class Posicion:
    """Esta clase hace los calculos para determinar la posicion en modo S"""
    def codifica_altitud(self, alt):
        # Funciona solo para altitudes menores a 50175 feet
        alt_bin=np.zeros(12, dtype=int)
        #Para altitudes -1000 <= X <= 50175 pies, establezca el bit 8 (el bit Q) en verdadero,
        #lo que significa una resolución de 25 pies
        if alt < 50175:
            alt_bin[7]=1
            N=int((int(alt) + 1000) / 25)
        else:
            alt_bin[7]=0
            N=int((int(alt) + 1000) / 100)
        a=bin(N)
        for i in range(len(a)-1):
            if (11-i) > 7:
                alt_bin[11-i]=a[len(a)-i-1]
            if (11-i) < 7:
                alt_bin[(11-i)]=a[len(a)-i]
        return (alt_bin)

    def Mod(self,x,y):

        return (x-y*math.floor(x/y))

    def NL(self,lat, NZ):
        if abs(lat) > 87.0:
            return (1)
        elif abs(lat) == 87.0:
            return (2)
        else:
            return math.floor((2.0*math.pi) * pow(math.acos(1.0- (1.0-math.cos(math.pi/(2.0*NZ)))/ math.cos( (math.pi/180.0)*abs(lat))**2 ),-1))

    def dlat(self,cpr,NZ):
        
        return ((360)/(4*NZ-cpr))

    def rlat(self, NB, YZ, lat, dlat):

        return(dlat*(YZ/pow(2,NB))+math.floor(lat/dlat))

    def dlon(self, x, cpr, NZ):
        
        return (360/max(self.NL(x,NZ)-cpr,1))
        

    def codificacion_cpr(self, lat, lon, cpr, NZ):
        #lat_bin brinda informacion de la latitud en binaria con formato cpr son los bits 55 a 71
        lat_bin=np.zeros(17, dtype=int)
        #lat_bin brinda informacion de la latitud en binaria con formato cpr son los bits 72 a 88
        lon_bin=np.zeros(17, dtype=int)

        dlati= self.dlat(cpr,NZ)
        #yz=math.floor(pow(2,17) * ((lat % dlati)/dlati) + 0.5)
        yz=math.floor(pow(2,17) * (self.Mod(lat,dlati)/dlati) + 0.5)
        rlat = dlati * ((yz / pow(2,17)) + math.floor(lat / dlati))
        
        

        dloni = self.dlon(rlat, cpr, NZ)
        #dloni = self.dlon(lon, cpr, NZ)
        #xz = math.floor(pow(2,17) * ((lon % dloni)/dloni) + 0.5)
        xz = math.floor(pow(2,17) * (self.Mod(lon,dloni)/dloni) + 0.5)

        a=bin(int(yz))
        for i in range(len(a)-2):
            lat_bin[16-i]=a[len(a)-i-1]

        b=bin(int(xz))
        for i in range(len(b)-2):
            lon_bin[16-i]=b[len(b)-i-1]

        return  (lat_bin , lon_bin)

class Identificacion:

    def ICAO(self,ICAO):
        b1=0
        b2=1
        b3=2
        b4=3
        #icaob tiene los datos en binarios del ICAO son los bits 9 al 32
        icaob=np.zeros(24, dtype=int)
        for i in range(len(ICAO)):
            if ICAO[i]=='0':
                icaob[b1]=0
                icaob[b2]=0
                icaob[b3]=0
                icaob[b4]=0
            if ICAO[i]=='1':
                icaob[b1]=0
                icaob[b2]=0
                icaob[b3]=0
                icaob[b4]=1
            if ICAO[i]=='2':
                icaob[b1]=0
                icaob[b2]=0
                icaob[b3]=1
                icaob[b4]=0
            if ICAO[i]=='3':
                icaob[b1]=0
                icaob[b2]=0
                icaob[b3]=1
                icaob[b4]=1
            if ICAO[i]=='4':
                icaob[b1]=0
                icaob[b2]=1
                icaob[b3]=0
                icaob[b4]=0
            if ICAO[i]=='5':
                icaob[b1]=0
                icaob[b2]=1
                icaob[b3]=0
                icaob[b4]=1
            if ICAO[i]=='6':
                icaob[b1]=0
                icaob[b2]=1
                icaob[b3]=1
                icaob[b4]=0
            if ICAO[i]=='7':
                icaob[b1]=0
                icaob[b2]=1
                icaob[b3]=1
                icaob[b4]=1
            if ICAO[i]=='8':
                icaob[b1]=1
                icaob[b2]=0
                icaob[b3]=0
                icaob[b4]=0
            if ICAO[i]=='9':
                icaob[b1]=1
                icaob[b2]=0
                icaob[b3]=0
                icaob[b4]=1
            if ICAO[i]=='A':
                icaob[b1]=1
                icaob[b2]=0
                icaob[b3]=1
                icaob[b4]=0
            if ICAO[i]=='B':
                icaob[b1]=1
                icaob[b2]=0
                icaob[b3]=1
                icaob[b4]=1
            if ICAO[i]=='C':
                icaob[b1]=1
                icaob[b2]=1
                icaob[b3]=0
                icaob[b4]=0
            if ICAO[i]=='D':
                icaob[b1]=1
                icaob[b2]=1
                icaob[b3]=0
                icaob[b4]=1
            if ICAO[i]=='E':
                icaob[b1]=1
                icaob[b2]=1
                icaob[b3]=1
                icaob[b4]=0
            if ICAO[i]=='F':
                icaob[b1]=1
                icaob[b2]=1
                icaob[b3]=1
                icaob[b4]=1
            b1=b1+4
            b2=b2+4
            b3=b3+4
            b4=b4+4
        b1=0
        b2=1
        b3=2
        b4=3
        return (icaob)

class Genera_crc:
    def generador_crc(self,mensaje):
        #G(x)=x^24+x^23+x^22+x^21+x^20+x^19+x^18+x^17+x^16+x^15+x^14+x^13+x^12+x^10+x^03+1
        polinomio_generador=[1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1]
        crc=np.zeros(24,dtype=int)
        for i in range(len(mensaje)-24):
            if mensaje[i] == 1:
                for j in range(len(polinomio_generador)):
                    mensaje[i+j]=mensaje[i+j]^polinomio_generador[j]
        crc = mensaje[-24:]
        return (crc)

class Airbone_Velocity:
    def Heading(self, heading):
        hdg=np.zeros(10, dtype=int)

        dec=int(heading*1024/360)
        #print(dec)
        a=bin(int(dec))
        #print(a)
        for i in range(len(a)-2):
            hdg[9-i]=a[len(a)-i-1]
        return (hdg)

    def Velocity(self,velocidad):
        AS=np.zeros(10,dtype=int)
        a=bin(int(velocidad))
        #print(velocidad)
        #print(a)
        for i in range(len(a)-2):
            AS[9-i]=a[len(a)-i-1]
        #print(AS)
        return (AS)

    def Vertical_Rate(self,vertical):
        Vr=np.zeros(9,dtype=int)
        if vertical < 0:
            S_vr=1
        else:
            S_vr=0
        dec=(vertical)/64 + 1
        a=bin(abs(dec)) 
        for i in range(len(a)-2):
            Vr[8-i]=a[len(a)-i-1]
        return (S_vr,Vr)

class gen_dec_to_bin(gr.sync_block):
    """
    docstring for block gen_dec_to_bin
    """
    def __init__(self, fs, file):
        gr.sync_block.__init__(self,
            name="gen_dec_to_bin",
            in_sig=None,
            out_sig=[np.int8])

        self.fs = fs
        assert self.fs % Tasa_Simbolos == 0, "ADS-B está diseñado para operar en un número entero de muestras por símbolo, no% f sps" % (self.fs / Tasa_Simbolos)
        self.rmuestras = int(fs // Tasa_Simbolos) #Calculo la relacion entre samp_rate y la Tasa de Simbolos
        self.file=file

        f = open(self.file, "rt")
        #cambiar la direccion del directorio por la que se quiera generar el arcivo csv
        f2=open("/home/marco/Documentos/Capturadec_to_bin.csv","wt")
        mensaje=f.readlines()

        for x in mensaje:
            x_separado=x.split(',')
            if x_separado[1]=='3':
                f2.write(x)
            elif x_separado[1]=='4':
                f2.write(x)
        f2.close()

        #cambiar la direccion del directorio por la que se quiera abrir el arcivo csv
        f3=open("/home/marco/Documentos/Capturadec_to_bin.csv","rt")
        self.mensaje=f3.readlines()
        self.contador = 0
        self.parity = 0
        self.trama4 = 0
        # Establezca el historial para que podamos verificar los preámbulos que envuelven el
        # fin de input_items de la llamada work () anterior [0]
        self.N_hist = bits_datos*self.rmuestras
        self.set_history(self.N_hist)

        # Propagar etiquetas
        self.set_tag_propagation_policy(gr.TPP_DONT)

    def work(self, input_items, output_items):
        out = output_items[0]
        
        ICAO=[]
        fligth=[]
        altitud=0
        heading=0
        velocidad=0
        vertical=0
        latitud=0
        longitud=0

        mod=0
        NZ=15
        NB=17

        #mensaje1 tiene los datos de DF (formato de bajada) y CA (identificador adicional) codificados
        #corresponde a (D) son los bits 1 8
        mensaje1=[1,0,0,0,1,1,1,0]
        #icaob tiene los datos en binarios del ICAO son los bits 9 al 32
        icaob=np.zeros(24, dtype=int)
        
        #SS provee informacion del estado de vigilancia corresponde a los bits 38 y 39
        SS=np.zeros(2, dtype=int)
        #NIC, NAC, NUC y SIL, esos acrónimos suenan confusos. Son números categóricos 
        #para la integridad, precisión o incertidumbre de las mediciones de posición
        # coreesponde al bit 40
        NICsb=0
        #alt_bin tiene la informacion correspondiente a la altitud corresponde a los bits 41 a 52
        alt_bin=np.zeros(12, dtype=int)
        #T da informaicon sobre el tiempo corresponde al bit 53
        T=0
        #cpr se utiliza para la icaob de latitud y longitud corresponde al bit 54
        cpr=0
        #lat_bin brinda informacion de la latitud en binaria con formato cpr son los bits 55 a 71
        lat_bin=np.zeros(17, dtype=int)
        #lat_bin brinda informacion de la latitud en binaria con formato cpr son los bits 72 a 88
        lon_bin=np.zeros(17, dtype=int)
        # paridad son los datos en binario resultado del crc son los bit 89 al 112 
        paridad=np.zeros(24, dtype=int)
        # ST son los bits 38-40 que indican el subtipo de la trama para codificar velocidad, en estecaso
        # sera subtipo de velocidad tipo 3
        ST=[0,1,1]
        # IC es el bit qe indica Bandera de cambio de intención es el bit numero 41
        IC=0
        # RESV_A bit reservado corresponde al bit 42 de la trama
        RESV_A=0
        # NAC o incertidumbre de velocidad corresponde a los bits 43 al 45
        NAC=[0,0,0]
        # S_hdg o heading status indica si el dato esta o no disponible corresponde al bit 46
        S_hdg=1
        # hdg de 10 bits representa la proporción de grados de un círculo completo, es decir, 360 grados.
        # (Nota: 0000000000-1111111111 representa 0-1023) son los bits 47 al 56
        hdg=np.zeros(10, dtype=int)
        #AS_t Para saber qué tipo de velocidad aerodinámica (TAS o IAS), primero debemos mirar el campo AS-t
        # es el bir 57
        AS_t=1
        # AS la velocidad es simplemente una conversión binaria a decimal de bits AS (en nudos) son los bits
        # 58 al 67
        AS=np.zeros(10,dtype=int)
        #El campo Fuente de frecuencia vertical (VrSrc) determina si se trata de una medición
        # en altitud de presión barométrica o altitud geométrica es el bit 68:
        VrSrc=0
        # La dirección del movimiento vertical de la aeronave se puede leer en el campo S_vr, en el mensaje bit-69
        S_vr=0
        #La velocidad vertical real Vr es el valor de los bits 70-78, menos 1, y luego se multiplica por 64 en pies / minuto (pies / min)
        Vr=np.zeros(9,dtype=int)
        #RESV_B bits reservados correspondientes a los bit 79 y 80
        RESV_B=[0,0]
        # S_Dif, Diferencia de baro alt, signo bit 81
        S_Dif=0
        # Dif Diferencia de baro alt bits del  82 al 88
        Diff=np.zeros(7,dtype=int)

        crc_generador=[1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1]
        calculo_crcp=np.zeros(112,dtype=int)
        calculo_crci=np.zeros(112,dtype=int)
        tramap=np.zeros(112, dtype=int)
        tramai=np.zeros(112, dtype=int)
        trama=np.zeros(112,dtype=int)

        N = len(output_items[0])
        pulso=np.zeros(N,dtype=int)

        samp = Posicion()
        iden = Identificacion()
        chequeo = Genera_crc()
        velo= Airbone_Velocity()

        if self.contador < len(self.mensaje)/22:
            #print(self.mensaje[1])
            x_separado=self.mensaje[self.contador].split(',')
            #print(x_separado[1])
            
            if x_separado[1]=='3':
                ICAO=x_separado[4]
                #print(len(ICAO))
                altitud=int(x_separado[11])
                latitud=float(x_separado[14])
                longitud=float(x_separado[15])
            #TC se utiliza saber que informacion se envia en la trama son los bits 33 al 37
            #TC=np.zeros(5, dtype=int)
            TC=[0,1,0,1,1]
            icaob=iden.ICAO(ICAO)
            alt_bin=samp.codifica_altitud(altitud)
            (lat_bin, lon_bin)=samp.codificacion_cpr(latitud, longitud, cpr, NZ)

            tramap[0:8]=mensaje1[:]
            tramap[8:32]=icaob[:]
            tramap[32:37]=TC[:]
            tramap[37:39]= SS[:]
            tramap[39]= NICsb
            tramap[40:52]=alt_bin[:]
            tramap[52]=T
            tramap[53]=cpr
            tramap[54:71]=lat_bin
            tramap[71:88]=lon_bin
            paridad[:]=chequeo.generador_crc(tramap)
            for i in range(24):
                tramap[88+i]=paridad[i]
            tramap[0:8]=mensaje1[:]
            tramap[8:32]=icaob[:]
            tramap[32:37]=TC[:]
            tramap[37:39]= SS[:]
            tramap[39]= NICsb
            tramap[40:52]=alt_bin[:]
            tramap[52]=T
            tramap[53]=cpr
            tramap[54:71]=lat_bin
            tramap[71:88]=lon_bin

            cpr=1
            (lat_bin, lon_bin)=samp.codificacion_cpr(latitud, longitud, cpr, NZ)
            tramai[0:8]=mensaje1[:]
            tramai[8:32]=icaob[:]
            tramai[32:37]=TC[:]
            tramai[37:39]= SS[:]
            tramai[39]= NICsb
            tramai[40:52]=alt_bin[:]
            tramai[52]=T
            tramai[53]=cpr
            tramai[54:71]=lat_bin
            tramai[71:88]=lon_bin
            paridad[:]=chequeo.generador_crc(tramai)
            for i in range(24):
                tramai[88+i]=paridad[i]
            tramai[0:8]=mensaje1[:]
            tramai[8:32]=icaob[:]
            tramai[32:37]=TC[:]
            tramai[37:39]= SS[:]
            tramai[39]= NICsb
            tramai[40:52]=alt_bin[:]
            tramai[52]=T
            tramai[53]=cpr
            tramai[54:71]=lat_bin
            tramai[71:88]=lon_bin
            
            if x_separado[1]=='4':
                ICAO=x_separado[4]
                heading=int(x_separado[13])
                velocidad=int(x_separado[12])
                vertical=int(x_separado[16])
                self.trama4=1
                TC=[1,0,0,1,1]
                icaob=iden.ICAO(ICAO)
                hdg=velo.Heading(heading)
                AS=velo.Velocity(velocidad)
                (S_vr,Vr)=velo.Vertical_Rate(vertical)
            trama[0:8]=mensaje1[:]
            trama[8:32]=icaob[:]
            trama[32:37]=TC[:]
            trama[37:40]=ST[:]
            trama[40]=IC
            trama[41]=RESV_A
            trama[42:45]=NAC[:]
            trama[45]=S_hdg
            trama[46:56]=hdg[:]
            trama[56]=AS_t
            trama[57:67]=AS[:]
            trama[67]=VrSrc
            trama[68]=S_vr
            trama[69:78]=Vr[:]
            trama[78:80]=RESV_B[:]
            trama[80]=S_Dif
            trama[81:88]=Diff
            paridad[:]=chequeo.generador_crc(trama)
            for i in range(24):
                trama[88+i]=paridad[i]
            trama[0:8]=mensaje1[:]
            trama[8:32]=icaob[:]
            trama[32:37]=TC[:]
            trama[37:40]=ST[:]
            trama[40]=IC
            trama[41]=RESV_A
            trama[42:45]=NAC[:]
            trama[45]=S_hdg
            trama[46:56]=hdg[:]
            trama[56]=AS_t
            trama[57:67]=AS[:]
            trama[67]=VrSrc
            trama[68]=S_vr
            trama[69:78]=Vr[:]
            trama[78:80]=RESV_B[:]
            trama[80]=S_Dif
            trama[81:88]=Diff
        
        
         
        if self.parity == 0 and self.trama4 ==0:
            for i in range(N):
                if i < 40:
                    pulso[i]=0
                if i == 40:
                    self.add_item_tag(
                       0,
                        (self.nitems_written(0)) +i,
                        pmt.to_pmt("burst"),
                        pmt.to_pmt("gen1")
                    )
                    pulso[i]=tramap[i-40]
                if i > 40 and i <= (40 + len(tramap)-1):
                    pulso[i]=tramap[i-40]
                if i > 40 + len(tramap) and i < N:
                    pulso[i]=0

        if self.parity == 1 and self.trama4 ==0:
            for i in range(N):
                if i < 40:
                    pulso[i]=0
                if i == 40:
                    self.add_item_tag(
                       0,
                        (self.nitems_written(0)) +i,
                        pmt.to_pmt("burst"),
                        pmt.to_pmt("gen1")
                    )
                    pulso[i]=tramai[i-40]
                if i > 40 and i <= (40 + len(tramai)-1):
                    pulso[i]=tramai[i-40]
                if i > 40 + len(tramai) and i < N:
                    pulso[i]=0

        if self.trama4 == 1:
            for i in range(N):
                if i < 40:
                    pulso[i]=0
                if i == 40:
                    self.add_item_tag(
                       0,
                        (self.nitems_written(0)) +i,
                        pmt.to_pmt("burst"),
                        pmt.to_pmt("gen1")
                    )
                    pulso[i]=trama[i-40]
                if i > 40 and i <= (40 + len(trama)-1):
                    pulso[i]=trama[i-40]
                if i > 40 + len(trama) and i < N:
                    pulso[i]=0
                    
        if self.parity == 0 and self.trama4 ==0:
            self.parity=1   
        elif self.parity == 1 and self.trama4==0:
            self.parity = 0
            self.contador=self.contador + 1
        elif self.trama4 == 1:
            self.contador= self.contador + 1
            self.trama4 = 0

        #signal = []
        #for bit in pulso:
        #    if bit == 1:
        #        I = 1
        #        Q = 1
        #    else:
        #        I = 0
        #        Q = 0
        #    signal.append(I)
        #    signal.append(Q)
                 
        out[:] = pulso[:]
        #out[:]= signal
        return len(output_items[0])

